import {StyleSheet, Text, View, Image} from 'react-native';
import React, {useEffect} from 'react';

const Splashscreen = ({navigation, route}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('AuthNavigation');
    }, 1000);
  }, []);
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#ffff',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Image
        source={require('../assets/image/splash.png')}
        style={{width: 150, height: 150, resizeMode: 'contain'}}
      />
    </View>
  );
};

export default Splashscreen;

const styles = StyleSheet.create({});
