import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

export default function Home({navigation, route}) {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingBottom: 10,
          paddingTop: 56,
        }}>
        <View
          style={{
            paddingHorizontal: 22,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Image
              source={require('../assets/image/profile.png')}
              style={{width: 50, height: 50, borderRadius: 10}}
            />
            <Image source={require('../assets/icon/bag.png')} />
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontSize: 17, color: 'black'}}>Hello, Bagas!</Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontSize: 27, fontWeight: 'bold', color: 'black'}}>
              Ingin merawat dan perbaiki sepatumu? Cari disini!
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 22,
              marginBottom: 20,
            }}>
            <View
              style={{
                backgroundColor: '#F6F8FF',
                flexDirection: 'row',
                borderRadius: 10,
                paddingHorizontal: 13,
                width: '83%',
              }}>
              <Image
                source={require('../assets/icon/search.png')}
                style={{alignSelf: 'center'}}
              />
              <TextInput style={{marginLeft: 10, width: '100%'}}></TextInput>
            </View>
            <View
              style={{
                backgroundColor: '#f6f8ff',
                borderRadius: 10,
                width: '13%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image source={require('../assets/icon/filter.png')} />
            </View>
          </View>
        </View>
        <View
          style={{
            backgroundColor: '#F6F8FF',
            paddingTop: 18,
            paddingHorizontal: 22,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View
              style={{
                backgroundColor: '#fff',
                padding: 15,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image source={require('../assets/icon/shoe.png')} />
              <View style={{width: 69, alignItems: 'center'}}>
                <Text
                  style={{
                    color: '#BB2427',
                    fontSize: 10,
                    marginTop: 5,
                    fontWeight: '600',
                  }}>
                  Sepatu
                </Text>
              </View>
            </View>
            <View
              style={{
                backgroundColor: '#fff',
                padding: 15,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image source={require('../assets/icon/jaket.png')} />
              <View style={{width: 69, alignItems: 'center'}}>
                <Text
                  style={{
                    color: '#BB2427',
                    fontSize: 10,
                    marginTop: 5,
                    fontWeight: '600',
                  }}>
                  Jaket
                </Text>
              </View>
            </View>
            <View
              style={{
                backgroundColor: '#fff',
                padding: 15,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image source={require('../assets/icon/tas.png')} />
              <View style={{width: 69, alignItems: 'center'}}>
                <Text
                  style={{
                    color: '#BB2427',
                    fontSize: 10,
                    marginTop: 5,
                    fontWeight: '600',
                  }}>
                  Tas
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              paddingTop: 27,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text style={{fontSize: 16, fontWeight: 'bold', color: '#0A0827'}}>
              Rekomendasi Terdekat
            </Text>
            <Text style={{color: '#E64C3C', fontWeight: '500'}}>View All</Text>
          </View>
          <View
            style={{
              marginTop: 24,
              backgroundColor: '#fff',
              padding: 6,
              flexDirection: 'row',
              borderRadius: 12,
            }}>
            <Image
              source={require('../assets/image/photo1.png')}
              style={{width: 85, height: 126, borderRadius: 5, marginRight: 2}}
            />
            <View style={{padding: 10}}>
              <View
                style={{
                  flexDirection: 'row',
                  width: '67.6%',
                  justifyContent: 'space-between',
                  marginBottom: 8,
                }}>
                <View>
                  <Image
                    source={require('../assets/icon/star.png')}
                    style={{
                      flexShrink: 0,
                      width: 60,
                      height: 10,
                      marginBottom: 3,
                    }}
                  />
                  <Text style={{fontSize: 13}}>4.8 ratings</Text>
                </View>
                <Image source={require('../assets/icon/heart.png')} />
              </View>
              <View style={{marginBottom: 3}}>
                <Text
                  style={{fontWeight: 'bold', color: 'black', fontSize: 16}}>
                  Jack Repair Gejayan
                </Text>
              </View>
              <Text style={{fontSize: 10}}>
                Jl. Gejayan III No.2, Karangasem, Kec. Laweyan...
              </Text>
              <View
                style={{
                  marginTop: 10,
                  backgroundColor: '#E64C3C33',
                  width: 63,
                  paddingHorizontal: 8,
                  paddingVertical: 3,
                  borderRadius: 10,
                  justifyContent: 'center',
                }}>
                <Text style={{color: '#EA3D3D', fontWeight: '700'}}>TUTUP</Text>
              </View>
            </View>
          </View>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('HomeNavigation', {screen: 'Detail'})
            }>
            <View
              style={{
                marginTop: 10,
                backgroundColor: '#fff',
                padding: 6,
                flexDirection: 'row',
                borderRadius: 12,
              }}>
              <Image
                source={require('../assets/image/photo2.png')}
                style={{
                  width: 85,
                  height: 126,
                  borderRadius: 5,
                  marginRight: 2,
                }}
              />
              <View style={{padding: 10}}>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '67.6%',
                    justifyContent: 'space-between',
                    marginBottom: 8,
                  }}>
                  <View>
                    <Image
                      source={require('../assets/icon/star.png')}
                      style={{
                        flexShrink: 0,
                        width: 60,
                        height: 10,
                        marginBottom: 3,
                      }}
                    />
                    <Text style={{fontSize: 13}}>4.7 ratings</Text>
                  </View>
                  <Image source={require('../assets/icon/heart1.png')} />
                </View>
                <View style={{marginBottom: 3}}>
                  <Text
                    style={{fontWeight: 'bold', color: 'black', fontSize: 16}}>
                    Jack Repair Seturan
                  </Text>
                </View>
                <Text style={{fontSize: 10}}>Jl. Seturan, Kec. Laweyan...</Text>
                <View
                  style={{
                    marginTop: 10,
                    backgroundColor: 'rgba(17, 168, 78, 0.12)',
                    width: 63,
                    paddingHorizontal: 8,
                    paddingVertical: 3,
                    borderRadius: 10,
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      width: '100%',
                      color: '#11A84E',
                      fontWeight: '700',
                    }}>
                    BUKA
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({});
