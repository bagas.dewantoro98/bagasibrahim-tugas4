import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
} from 'react-native';

export default function Detail({navigation, route}) {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <ImageBackground
          source={require('../assets/image/photo2.png')} //load atau panggil asset image dari local
          style={{
            width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
            height: 317,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 25,
              marginHorizontal: 25,
            }}>
            <TouchableOpacity onPress={() => navigation.navigate('BotNavBar')}>
              <Image source={require('../assets/icon/arrback.png')} />
            </TouchableOpacity>
            <Image source={require('../assets/icon/bag2.png')} />
          </View>
        </ImageBackground>
        <View
          style={{
            width: '100%',
            backgroundColor: 'white',
            borderTopLeftRadius: 19,
            borderTopRightRadius: 19,
            paddingHorizontal: 20,
            paddingTop: 27,
            marginTop: -20,
          }}>
          <Text
            style={{
              color: 'black',
              fontWeight: 'bold',
              fontSize: 27,
              marginBottom: 5,
            }}>
            Jack Repair Seturan
          </Text>
          <Image source={require('../assets/icon/star.png')} />
          <View
            style={{
              flexDirection: 'row',
              marginTop: 13,
              marginLeft: -5,
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Image source={require('../assets/icon/location.png')} />
            <Text style={{fontSize: 14, marginLeft: -45}}>
              Jalan Affandi (Gejayan), No. 15, Sleman{'\n'}Yogyakarta, 55384
            </Text>
            <Text style={{fontWeight: 'bold', fontSize: 17, color: '#3471CD'}}>
              Lihat Maps
            </Text>
          </View>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
            <View
              style={{
                backgroundColor: '#11A84E1F',
                paddingVertical: 4,
                paddingHorizontal: 13,
                borderRadius: 20,
              }}>
              <Text style={{fontSize: 20, fontWeight: '700', color: '#11A84E'}}>
                BUKA
              </Text>
            </View>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 20,
                marginLeft: 15,
                color: 'black',
              }}>
              09:00 - 21:00
            </Text>
          </View>
        </View>
        <View
          style={{
            borderColor: '#EEEEEE',
            borderWidth: 1,
            marginTop: 15,
          }}></View>
        <View
          style={{
            width: '100%',
            backgroundColor: 'white',
            paddingTop: 25,
            paddingHorizontal: 20,
          }}>
          <Text style={{fontSize: 23, color: 'black'}}>Deskripsi</Text>
          <Text style={{marginTop: 8, fontSize: 17}}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
            gravida mattis arcu interdum lectus egestas scelerisque. Blandit
            porttitor diam viverra amet nulla sodales aliquet est. Donec enim
            turpis rhoncus quis integer. Ullamcorper morbi donec tristique
            condimentum ornare imperdiet facilisi pretium molestie.
          </Text>
          <Text style={{fontSize: 23, color: 'black', marginTop: 20}}>
            Range Biaya
          </Text>
          <Text style={{fontSize: 20, marginTop: 8}}>
            Rp 20.000 - Rp 80.000
          </Text>
          <TouchableOpacity
            style={{
              backgroundColor: '#BB2427',
              marginTop: 30,
              alignItems: 'center',
              paddingVertical: 17,
              borderRadius: 12,
            }}>
            <Text style={{color: 'white', fontSize: 25, fontWeight: 'bold'}}>
              Repair Disini
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({});
