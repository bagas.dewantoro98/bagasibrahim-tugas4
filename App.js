import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Routing from './src/Routing';
import Detail from './src/screen/Detail';

const App: () => Node = () => {
  return (
    <View style={{flex: 1}}>
      <Routing />
    </View>
  );
};

const styles = StyleSheet.create({});

export default App;
